import React from "react";
import "./Login.css"
class Login extends React.Component{
    state={
        name:"",
        password:""
    }
    handleChange = (e) => {
        const {name,value} = e.target;
        this.setState({[name]:value})
    }
    handleSubmit = (e) => {  
        e.preventDefault();  
        let errors = {};    
       
        const user_details = this.state;
        console.log(user_details);

        if(user_details.name !== "harsh" || user_details.password !== "1234"){
            errors="Enter Correct Username Password";
        }else{
            errors = "";
            this.props.isLogin(true);
        }
        this.setState({
            errors: errors
        });
        
        
    }
    render(){
        return(
            <div className="div-login">
                <form onSubmit={this.handleSubmit}>
                    <h1>Login Form</h1>
                    <input type="name" name="name" placeholder="Enter UserName" required onChange={this.handleChange}/>
                    <input type="password" name="password" placeholder="Enter Password" required onChange={this.handleChange}/>
                    <div className="text-danger">{this.state.errors}</div>
                    <button onSubmit={this.handleSubmit}>Login</button>
                </form>
            </div>
        )
    }
}

export default Login;