import React from "react";
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Login from "./Compnent/Login";
import Home from "./Home/Home";

class App extends React.Component{
  state={ 
    isLog:false
  }

  handleLogin=(isLog) => this.setState({isLog})

  render(){ 
    
    const {isLog} = this.state;   

    return (
      <div className="App">
        <BrowserRouter>
          <Switch>
            <Route exact path="/" render={() => !isLog ? <Login isLogin={this.handleLogin}/>:<Home/> } />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
